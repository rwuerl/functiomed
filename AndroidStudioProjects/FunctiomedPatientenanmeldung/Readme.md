# Android Studio Shorcuts
Shift+Alt+Pfeil Zeile verschieben
CTRL+ALT+L Formatieren
ALT+F6 Rename
CTRL+ALT+V Markierung als Variable erstellen
CTRL+SHIFT UP verschiebt Methode nach oben

# Links:
SVG Editor für Logo: https://editor.method.ac/
Icon: http://romannurik.github.io/AndroidAssetStudio/icons-launcher.html#foreground.type=image&foreground.space.trim=1&foreground.space.pad=0.15&foreColor=rgba(96%2C%20125%2C%20139%2C%200)&backColor=rgb(68%2C%20138%2C%20255)&crop=0&backgroundShape=square&effects=none&name=ic_launcher
UI Sizeing: https://stackoverflow.com/questions/45264046/design-looks-perfect-on-phone-but-so-small-on-tablet-android-studio
MuPDF Bug: https://bugs.ghostscript.com/show_bug.cgi?id=703808

#Pendenzen
Log4j1 Logfile 
SPEN Unterstützung

#Auslieferung
Inkrement der Version im Filenamen von /Benutzer-Handbuch Digitale Unterschrift V1.x.docx
Inkrement der Version auf Seite 1 im Benutzer-Handbuch 
Increment versionMinor /app/build.gradle
Increment der Version im /SaveAPK.cmd
Releasenotes weiter unten schreiben
Testen mit Menu Build Clean & Make Project & SaveAPK.cmd
Commit & Push "Version 1.5 Vorbereitung"
Setzen svn_revision /app/build.gradle (Git Rev auf Bitbucket holen)
Menu Build: Clean & Make Project
/SaveAPK.cmd ausführen
APK zum Download bereitstellen
Commit  & Push "Version 1.5 08eeb77d"


#Releasenotes 
1.0 Erste Auslieferung
1.1 Roter Löschen Button
1.2 Tabletname in Einstellungen und in Ausgabefile aufnehmen
1.3 Ausgabe PDF direkt in das Root schreiben anstatt in das Unterverzeichnis Fomulare
1.4 Mehrseitiges PDF. Fix korruptes PDF im Netzwerk wenn auch lokal gespeichert wird.
1.5 Navigation unten ausblenden. Nach Resume kann nicht weiter geschrieben werden (Stift deaktiviert). Korrupte PDF.


/**
 * Bug 703808 - Android Devkit 1.18.1.1: Small draw annotations like dots and points are not recognized nor displayed nor saved
 * Diese Klasse DocPdfPageView.java wurde im /libs/editor.aar/classes.jar gelöscht und hier in den Projekt Sourcen aufgenommen.
 * Der Fix ist "getMinInkSize() return 0". Wird evtl. in 1.18.1.2 obsolete und muss wieder gelöscht werden.
 * Details siehe Bugzilla https://bugs.ghostscript.com/show_bug.cgi?id=703808
 */

/**
 * Bug xxxxxx - Android Devkit 1.18.1.1: AlertDialog Buttons not displayed
 * Diese Klasse NUIDocView.java wurde im /libs/editor.aar/classes.jar gelöscht und hier in den Projekt Sourcen aufgenommen.
 * Der Fix ist mCanGoBack = false in Zeile 3263 damit der Dialog ohne Buttons nicht angezeigt wird. Der Unsichtbare Ok Button speichert in mein Template!
 */