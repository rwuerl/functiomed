package ch.functiomed.patientenanmeldung;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import com.artifex.solib.ArDkLib;
import com.artifex.solib.SODocSaveListener;
import com.artifex.sonui.editor.DocumentListener;
import com.artifex.sonui.editor.DocumentView;
import com.artifex.sonui.editor.Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

/*
 * Hauptseite mit dem PDF und Button Speichern.
 * <p>
 * MuPDF Developer Documentation https://mupdf.com/appkit/docs/
 * Anwendungsbeispiel zum Download: https://github.com/ArtifexSoftware/mupdf-android-appkit/releases/tag/1.18.1.1
 * <p>
 * Hinweise:
 * https://stackoverflow.com/questions/11254523/android-runonuithread-explanation
 */
public class FunctiomedActivity extends AppCompatActivity {

    /**
     * App Berechtigungscode
     */
    private static final int PERMISSION_STORAGE = 1;
    private float mOriginalScale = -1;
    /**
     * URI des PDF
     */
    private Uri myCurrentDocumentUri = null;
    /**
     * MuPDF GUI Widget
     */
    private DocumentView myDocumentView = null;

    /**
     * Die PDF URI wurde gesetzt und kann nun angezeigt werden.
     */
    private void createMuPdfUI() {

        // GUI muss nochmals initialisiert werden (myDocumentView.start kann nur einmal ausgeführt werden)
        setContentView(R.layout.activity_functiomed);
        myDocumentView = findViewById(R.id.doc_view);

        // ProgressBar abschalten
        ProgressBar pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.GONE);

        // Sprache anpassen (Internationalisierung mit Locale=EN ist schwer)
        Button speichernButton = findViewById(R.id.speichern);
        Button neueAnmeldungButton = findViewById(R.id.neueAnmeldung);
        if (MyUtils.isLanguageDE) {
            // Speichern
            speichernButton.setText("Speichern und schliessen");
            speichernButton.requestLayout();
            // neueAnmeldungButton
            neueAnmeldungButton.setText("Löschen");
            neueAnmeldungButton.requestLayout();
        } else {
            // Speichern
            speichernButton.setText("Save and close");
            speichernButton.requestLayout();
            // neueAnmeldungButton
            neueAnmeldungButton.setText("Clear");
            neueAnmeldungButton.requestLayout();
        }

        // Stiftbreite aus den Einstellungen setzen
        ArDkLib.getAppConfigOptions().setDefaultPdfInkAnnotationDefaultLineThickness(MyUtils.settings_stiftbreite);
        Log.d("settings_stiftbreite ", String.valueOf(MyUtils.settings_stiftbreite));

        myDocumentView.setDocConfigOptions(ArDkLib.getAppConfigOptions());
        myDocumentView.setDocDataLeakHandler(Utilities.getDataLeakHandlers());

        // set a listener for document events
        myDocumentView.setDocumentListener(new DocumentListener() {

            @Override
            public void onDocCompleted() {
                // called when the document is done loading.
                Log.d("DocumentListener", "onDocCompleted pages " + myDocumentView.getPageCount());
            }

            @Override
            public void onPageLoaded(int pagesLoaded) {
                // called when another page is loaded from the document.
                if (mOriginalScale == -1)
                    mOriginalScale = myDocumentView.getScaleFactor();
                Log.d("DocumentListener", "onPageLoaded pages " + myDocumentView.getPageCount());
                // Stift einschalten.
                // Darf nur einmal gemacht werden bei mehreren Seiten!
                if (myDocumentView.getPageCount() == 1 ){
                    myDocumentView.setDrawModeOn();
                }
            }

            @Override
            public void onPasswordRequired() {
                // called when a password is required.
                Log.d("DocumentListener", "onPasswordRequired");
                // handlePassword();
            }

            @Override
            public void onViewChanged(float scale, int scrollX, int scrollY, Rect selectionRect) {
                // called when the scale, scroll, or selection in the document changes.
                Log.d("DocumentListener", "onViewportChanged");
            }
        });

        // set a listener for when the DocumentView is closed.
        // typically you'll want to close your activity.
        myDocumentView.setOnDoneListener(new DocumentView.OnDoneListener() {

            @Override
            public void done() {
                Log.d(MyUtils.MY_LOG_TAG, "setOnDoneListener");
                //FunctiomedActivity.super.finish();
            }
        });

        // open it, specifying showUI = false;
        myDocumentView.start(myCurrentDocumentUri, 0, false);

    }

    /**
     * Sucht den nächsten freien Datei Namen
     *
     * @param pList Liste der bestehenden Dateien
     * @return Der nächste freie Datei Name AnmeldeformularDE-XY.pdf
     */
    private String getFreeFilename(List<String> pList) {
        String result = "Anmeldeformular-XY.pdf";
        Log.i(MyUtils.MY_LOG_TAG, "Erhalte " + pList);
        // Finde den nächsten freien Filenamen auf dem Share
        int nextIndex = 1;
        for (int i = 0; i <= pList.size() - 1; i++) {
            if (pList.get(i).contains(MyUtils.ANMELDEFORMULAR_NAME + MyUtils.getLanguageString() + "-" + MyUtils.settings_tabletname + "-")) {
                String docName = pList.get(i);
                int indexBindestrich = docName.lastIndexOf("-");
                int indexPunkt = docName.indexOf(".");
                if (indexBindestrich > 1 && indexPunkt > 1) {
                    String docLaufnummer = docName.substring(indexBindestrich + 1, indexPunkt);
                    try {
                        int docIndex = Integer.parseInt(docLaufnummer);
                        // Den höheren Wert merken
                        nextIndex = (docIndex >= nextIndex) ? docIndex + 1 : nextIndex;
                    } catch (NumberFormatException e) {
                        Log.i(MyUtils.MY_LOG_TAG, "NumberFormatException docLaufnummer '" + docLaufnummer + "' ist keine Zahl.");
                        continue;
                    }
                }
            }
        }
        result = MyUtils.ANMELDEFORMULAR_NAME + MyUtils.getLanguageString() + "-" + MyUtils.settings_tabletname + "-" + nextIndex + ".pdf";
        Log.i(MyUtils.MY_LOG_TAG, "Liefere " + result);
        return result;
    }

    /**
     * Start der PDF Anzeige. Aufruf aus onCreate() und onRequestPermissionsResult()
     */
    private void loadTemplate() {
        // Already granted, proceed.

        // Ein Speicherort muss in den Settings gesetzt sein. Sonst Dialog anzeigen.
        if (!MyUtils.settings_isSave_local && !MyUtils.settings_isSave_network) {
            showMessage("Bitte einen Speicherort in den Einstellungen auswählen.", true);
        }

        // PDF laden und MuPDF öffnen
        Log.i(MyUtils.MY_LOG_TAG, "onNeueAnmeldung");
        if (MyUtils.settings_template_location.equals("Tablet")) {
            loadTemplateFromTablet();
        }
        if (MyUtils.settings_template_location.equals("Netzwerk")) {
            loadTemplateFromNetzwerk();
        }
    }

    /**
     * PDF in Thread vom Netzwerk laden, URI speichern und MuPDF öffnen.
     */
    private void loadTemplateFromNetzwerk() {
        Log.i(MyUtils.MY_LOG_TAG, "loadTemplateFromNetzwerk()");
        // Netzwerk Zugriffe sind im UI Thread nicht erlaubt.
        // GUI Zugriffe sind im separaten Thread nicht erlaubt.
        //Erstelle zuerst einen neuen Thread um das Template PDF zu holen.
        Runnable runnable = () -> {

            try {
                // Vorlagen Verzeichnis erstellen falls es noch nicht existiert
                SmbFile smbDir = MyUtils.getSmbFile(MyUtils.VORLAGEN_DIR + "/");
                if (!smbDir.exists()) {
                    smbDir.mkdirs();
                }

                SmbFile vorlageSmbFile = MyUtils.getSmbFile(MyUtils.VORLAGEN_DIR + "/" + MyUtils.getTemplateName());
                Log.i(MyUtils.MY_LOG_TAG, "Vorlage URL " + vorlageSmbFile.getURL());
                if (vorlageSmbFile.exists()) {
                    Log.i(MyUtils.MY_LOG_TAG, "Vorlage auf Netzlaufwerk gefunden.");

                    // Lokales Cache Verzeichnis erstellen
                    File cacheDir = MyUtils.getFile(MyUtils.CACHE_DIR);
                    if (!cacheDir.exists()) {
                        cacheDir.mkdirs();
                    }
                    // Lokale Kopie der Vorlage erstellen
                    File vorlageCacheFile = MyUtils.getFile(MyUtils.CACHE_DIR + File.separator + "temp_cache_in_" + MyUtils.getTemplateName());
                    Log.i(MyUtils.MY_LOG_TAG, "vorlageCacheFile " + vorlageCacheFile.getPath());


                    //Copy
                    InputStream in = new SmbFileInputStream(vorlageSmbFile);
                    OutputStream out = new FileOutputStream(vorlageCacheFile);
                    byte[] buf = new byte[MyUtils.BYTE_BUFFER];
                    int len;
                    int index = 0;
                    long start = System.currentTimeMillis();
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                        Log.i(MyUtils.MY_LOG_TAG, "copying " + MyUtils.BYTE_BUFFER + " (" + index++ + ")");
                    }
                    long finish = System.currentTimeMillis();
                    long timeElapsed = finish - start;
                    Log.i(MyUtils.MY_LOG_TAG, "Duration " + timeElapsed);
                    in.close();
                    out.close();
                    myCurrentDocumentUri = Uri.fromFile(vorlageCacheFile);
                    Log.i(MyUtils.MY_LOG_TAG, "myCurrentDocumentUri " + myCurrentDocumentUri);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            createMuPdfUI();
                        }
                    });
                } else {
                    // Erstelle eine Semaphore Datei
                    Log.i(MyUtils.MY_LOG_TAG, "Erstelle eine Semaphore Datei.");
                    SmbFile semaphoreSmbFile = MyUtils.getSmbFile(MyUtils.VORLAGEN_DIR + "/AnmeldeformularDE.pdf-bitte-hier-bereitstellen.txt");
                    semaphoreSmbFile.createNewFile();
                    // GUI Updates dürfen nur im UI Thred gemacht werden.
                    // runOnUiThread wird in die Event Queue vom UI Thread abgelegt und dor ausgeführt
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showMessage("Anmeldeformular" + MyUtils.getLanguageString() + ".pdf fehlt auf dem Netzlaufwerk.\n\n" + semaphoreSmbFile.getParent(), true);
                        }
                    });


                }
            } catch (IOException e) {
                Log.e(MyUtils.MY_LOG_TAG, "SMB Failed\n\n" + e.getMessage());
                e.printStackTrace();
                // GUI Updates dürfen nur im UI Thred gemacht werden.
                // runOnUiThread wird in die Event Queue vom UI Thread abgelegt und dor ausgeführt
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showMessage("Zugriff auf Server fehlgeschlagen.\n\nBitte prüfen ob das Tablet mit dem richtigen Netzwerk verbunden ist.\n\n" + e.getMessage(), true);
                    }
                });
            }
        };
        Thread t = new Thread(runnable);
        t.start();
    }

    /**
     * PDF vom Tablet laden, URI speichern und MuPDF öffnen.
     */
    private void loadTemplateFromTablet() {
        Log.i(MyUtils.MY_LOG_TAG, "loadTemplateFromTablet()");
        File f = MyUtils.getFile(MyUtils.VORLAGEN_DIR + File.separator + MyUtils.getTemplateName());
        Log.i(MyUtils.MY_LOG_TAG, "templatefile f.getAbsolutePath " + f.getAbsolutePath());
        if (f.exists()) {
            // Template ist vorhanden
            Log.i(MyUtils.MY_LOG_TAG, "Template ist vorhanden");
            myCurrentDocumentUri = Uri.fromFile(f);
            Log.i(MyUtils.MY_LOG_TAG, "myCurrentDocumentUri " + myCurrentDocumentUri);
            createMuPdfUI();

        } else {
            // Kein Template vorhanden. Pfad und Semaphore Datei erzeugen sowie Dialog
            // anzeigen.
            Log.i(MyUtils.MY_LOG_TAG, "Kein Template vorhanden. Erstelle Semaphore Datei");
            File semaphoreFile = MyUtils.getFile(MyUtils.VORLAGEN_DIR + File.separator + "AnmeldeformularDE.pdf-bitte-hier-bereitstellen.txt");
            try {
                semaphoreFile.getParentFile().mkdirs();
                semaphoreFile.createNewFile();
                showMessage("Anmeldeformular" + MyUtils.getLanguageString() + ".pdf fehlt im /Download/Secure/Functiomed/Vorlagen Verzeichnis.\n\nBitte Tablet am PC anschliessen und Vorlagen „AnmeldeformularDE.pdf“ und „AnmeldeformularEN.pdf“ kopieren.", true);
            } catch (IOException e) {
                Log.e(MyUtils.MY_LOG_TAG, "Error bei Erstellung Semaphore Datei in createNewFile() Path " + semaphoreFile.getPath());
                e.printStackTrace();
            }

        }
    }

    /**
     * Weitergabe an MuPDF
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (myDocumentView != null)
            myDocumentView.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Weitergabe an MuPDF
     */
    @Override
    public void onBackPressed() {
        if (myDocumentView != null)
            myDocumentView.onBackPressed();
    }

    /**
     * Weitergabe an MuPDF
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (myDocumentView != null)
            myDocumentView.onConfigurationChange(newConfig);
    }

    /**
     * Main Methode der Activity.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(MyUtils.MY_LOG_TAG, "Start FunctiomedActivity");
        super.onCreate(savedInstanceState);

        // App Einstellungen holen und in MyAppUtils für späteren Zugriff ablegen
        Log.i(MyUtils.MY_LOG_TAG, "Update der Einstellungen");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        MyUtils.settings_template_location = sharedPreferences.getString("template_location", "Tablet");
        MyUtils.settings_isSave_local = sharedPreferences.getBoolean("save_local", true);
        MyUtils.settings_isSave_network = sharedPreferences.getBoolean("save_network", false);
        MyUtils.settings_tabletname = sharedPreferences.getString("tabletname", "Tablet1");
        MyUtils.settings_hostname = sharedPreferences.getString("hostname", "n.a");
        MyUtils.settings_freigabe = sharedPreferences.getString("freigabe", "n.a");
        MyUtils.settings_username = sharedPreferences.getString("username", "n.a");
        MyUtils.settings_password = sharedPreferences.getString("password", "n.a");
        MyUtils.settings_domain = sharedPreferences.getString("domain", ""); // Muss leer sein wenn keine Domäne verfügbar ist.
        MyUtils.settings_stiftbreite = Float.parseFloat(sharedPreferences.getString("stiftbreite", "2.0f"));

        // Initialisiert die MuPDF Lib
        MainActivity.setupApplicationSpecifics(this);

        // Erste GUI Initialisierung nur mit ProgressBar. Zweite Initialisierung in createMuPdfUI()
        setContentView(R.layout.activity_functiomed);

        // ProgressBar anzeigen
        ProgressBar pb = findViewById(R.id.progressBar);
        pb.setVisibility(ProgressBar.VISIBLE);

        // Save Button entfernen damit nur die ProgressBar angezeigt wird.
        Button speichernButton = findViewById(R.id.speichern);
        speichernButton.setVisibility(Button.GONE);

        // Check permissions
        boolean askPermission = false;

        // Habe ich schon Berechtigung?
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            askPermission = true;
        }

        // Berechtigung mit Code 1 anfragen
        if (askPermission) {
            // Not immediately granted, so ask.
            // We'll return in onRequestPermissionsResult()
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_STORAGE);
            return;
        }

        // Falls aus einer anderen App mit dieser App ein PDF geöffnet wird
        Uri u = getIntent().getData();
        // content://com.sec.android.app.myfiles.FileProvider/device_storage/0/Download/Secure/Functiomed/Formulare/AnmeldeformularDE-4.pdf
        if (u != null) {
            Log.i(MyUtils.MY_LOG_TAG, "Uri received from Intent Data " + u);
            myCurrentDocumentUri = u;
            createMuPdfUI();
        } else {
            loadTemplate();
        }


    }

    /**
     * Weitergabe an MuPDF
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myDocumentView != null)
            myDocumentView.onDestroy();
    }

    /**
     * Weitergabe an MuPDF
     */
    @Override
    public void onPause() {
        if (myDocumentView != null)
            myDocumentView.onPause(new Runnable() {

                @Override
                public void run() {
                    // called when pausing is complete
                }
            });
        super.onPause();
    }

    /**
     * Antwort auf die Anfrage an den Benutzer (in onCreate()) ob diese App schreiben darf.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission was granted.
                    loadTemplate();
                } else {
                    // Permission denied.
                    Log.i(MyUtils.MY_LOG_TAG, "Permission denied from user!");
                    finish();
                }
                return;
            }
        }
    }

    /**
     * Weitergabe an MuPDF. Stift wieder einschalten.
     */
    @Override
    protected void onResume() {
        Log.i(MyUtils.MY_LOG_TAG, "onResume");
        super.onResume();
        if (myDocumentView != null) {
            myDocumentView.onResume();
            myDocumentView.setDrawModeOn();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(MyUtils.MY_LOG_TAG, "onStart()");
        //Navigation entfernen
        MyUtils.hideNavbar(this);
    }

    /**
     * Called when the user taps the neueAnmeldung Button
     */
    public void onNeueAnmeldung(View view) {
        Log.i(MyUtils.MY_LOG_TAG, "onNeueAnmeldung");
        if (myDocumentView != null) {
            myDocumentView.onDestroy();
            myDocumentView = null;
        }

        loadTemplate();
    }

    /**
     * Called when the user taps the Save Button
     */
    public void onSpeichern(View view) {
        Log.i(MyUtils.MY_LOG_TAG, "onSpeichern");
        if (myDocumentView != null) {
            try {
                // Stift abschalten sonst kommt die Handschrift Annotation nicht in das PDF
                Log.i(MyUtils.MY_LOG_TAG, "Stift automatisch abschalten");
                myDocumentView.setDrawModeOff(); // Fehler "cycle in tree (parents)" beim abschalten des Stifts!

                // ProgressBar anzeigen
                ProgressBar pb = findViewById(R.id.progressBar);
                pb.setVisibility(ProgressBar.VISIBLE);

                // Lokales Ausgabe Verzeichnis festlegen mit Basis Pfad
                // /storage/emulated/0/Download/Secure/Functiomed
                File baseDir = MyUtils.getFile("");

                // Liste der bestehenden Dateien erstellen
                List<String> fileList = Arrays.asList(baseDir.list());

                // Anhand Liste den nächsten freien Dateinamen ermitteln
                File outFile = MyUtils.getFile(getFreeFilename(fileList));

                // Save as lokal
                Log.i(MyUtils.MY_LOG_TAG, "Speichere nach Path " + outFile.getPath());
                myDocumentView.saveTo(outFile.getPath(), new SODocSaveListener() {

                    @Override
                    public void onComplete(int result, int err) {
                        if (result == SODocSave_Succeeded) {
                            // success
                            Log.i(MyUtils.MY_LOG_TAG, "Start onComplete(). Lokal wurde erfolgreich gespeichert.");

                            // Save as .. auf den Windows-Share im Netzwerk
                            if (MyUtils.settings_isSave_network) {
                                Log.i(MyUtils.MY_LOG_TAG, "Start Netzwerk Speicherung");

                                // Netzwerkzugriff in eigenem Thread
                                Runnable runnable = () -> {
                                    try {

                                        // Verzeichnis erstellen falls nicht vorhanden
                                        SmbFile smbBaseDir = MyUtils.getSmbFile("");
                                        if (!smbBaseDir.exists()) {
                                            smbBaseDir.mkdirs();
                                        }

                                        // Liste der bestehenden Dateien erstellen
                                        List<String> smbFileList = Arrays.asList(smbBaseDir.list());

                                        // Anhand Liste nächsten freien Dateinamen ermitteln
                                        SmbFile smbOutFile = MyUtils.getSmbFile(getFreeFilename(smbFileList));
                                        Log.i(MyUtils.MY_LOG_TAG, "Speichere nach Netzwerk Pfad " + smbOutFile.getPath());

                                        //Copy
                                        InputStream in = new FileInputStream(outFile);
                                        OutputStream out = new SmbFileOutputStream(smbOutFile);

                                        byte[] buf = new byte[MyUtils.BYTE_BUFFER];
                                        int len;
                                        int index = 0;
                                        long start = System.currentTimeMillis();
                                        while ((len = in.read(buf)) > 0) {
                                            out.write(buf, 0, len);
                                            Log.i(MyUtils.MY_LOG_TAG, "copying " + MyUtils.BYTE_BUFFER + " bytes. Index " + index++ + ")");
                                        }
                                        long finish = System.currentTimeMillis();
                                        long timeElapsed = finish - start;
                                        Log.i(MyUtils.MY_LOG_TAG, "Duration " + timeElapsed + " ms");
                                        in.close();
                                        out.close();

                                        // Lokales File löschen?
                                        if (!MyUtils.settings_isSave_local) {
                                            Log.i(MyUtils.MY_LOG_TAG, "Delete local File: " + outFile.getPath());
                                            outFile.delete();
                                        }

                                        // Rückmeldung
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                pb.setVisibility(ProgressBar.GONE);
                                                if (MyUtils.isLanguageDE) {
                                                    showMessage("Erfolgreich gespeichert.", true);
                                                } else {
                                                    showMessage("Successfully saved.", true);
                                                }
                                            }
                                        });

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                };
                                Thread t = new Thread(runnable);
                                t.start();

                                // Warten bis im Thread fertig gespeichert wurde
                                Log.i(MyUtils.MY_LOG_TAG, "Start: Warten bis fertig gespeichert wurde");
                                try {
                                    t.join();
                                } catch (InterruptedException e) {
                                    Log.e(MyUtils.MY_LOG_TAG, "Konnte nicht auf Thread für Netzwerkspeicherung warten! " + e.getMessage());
                                }
                                Log.i(MyUtils.MY_LOG_TAG, "End: Warten bis fertig gespeichert wurde");
                            }
                            pb.setVisibility(ProgressBar.GONE);
                            // Nur melden wenn es nicht auch noch zusätzlich auf dem Netzwerk gespeichert wird
                            if (!MyUtils.settings_isSave_network) {
                                if (MyUtils.isLanguageDE) {
                                    showMessage("Erfolgreich gespeichert.", true);
                                } else {
                                    showMessage("Successfully saved.", true);
                                }
                            }
                        } else {
                            // error
                            final String msg = "Beim Speichern des Formulars ist ein Fehler aufgetreten. Error " + err;
                            pb.setVisibility(ProgressBar.GONE);
                            myDocumentView.setDrawModeOn();
                            if (MyUtils.isLanguageDE) {
                                Log.e(MyUtils.MY_LOG_TAG, msg);
                                showMessage(msg, false);
                            } else {
                                Log.e(MyUtils.MY_LOG_TAG, "There was an error saving the form. Error " + err);
                                showMessage(msg, false);
                            }
                        }
                    }
                });

            } catch (Throwable ex) {
                Log.i(MyUtils.MY_LOG_TAG, "MuPDF RunTimeException!! " + ex.getMessage());
                ex.printStackTrace();
                // nochmals abschalten
                myDocumentView.setDrawModeOff();
                if (MyUtils.isLanguageDE) {
                    showMessage("Die Speicherung war leider unvollständig.\n\nBitte Dokument überprüfen und nochmals speichern.", false);
                } else {
                    showMessage("Unfortunately, the storage process was incomplete.\n\nPlease check the document and save it again. ", false);
                }
                ex.printStackTrace();
                // und wieder einschalten
                myDocumentView.setDrawModeOn();

            }
        }
        Log.i(MyUtils.MY_LOG_TAG, "End onSpeichern");
    }

    /**
     * Infodialog anzeigen
     *
     * @param message        text für die Anzeige
     * @param pCloseActivity FunctiomedActivity danach schliessen
     */
    private void showMessage(String message, boolean pCloseActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(message).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
                if (pCloseActivity) {
                    finish();
                }
            }
        }).show();
    }
}
