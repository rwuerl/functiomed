package ch.functiomed.patientenanmeldung;

import android.util.Log;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;

import com.artifex.solib.ArDkLib;

import java.io.File;
import java.net.MalformedURLException;

import jcifs.CIFSContext;
import jcifs.context.SingletonContext;
import jcifs.smb.NtlmPasswordAuthenticator;
import jcifs.smb.SmbFile;

/**
 * Statische Daten und Methoden für alle Activities.
 */
public class MyUtils {

    public final static String ANMELDEFORMULAR_NAME = "Anmeldeformular";
    /**
     * Grösse des Buffers in Byte für die Netzwerk Übertragung.
     * 22 * 1024 sind 22 Kilobytes. 64 KB wäre optimal für grössere Files.
     */
    public final static int BYTE_BUFFER = 22 * 1024;
    public final static String CACHE_DIR = "Cache";
    public final static String FUNCTIOMED_DIR = "Functiomed";
    public final static String MY_LOG_TAG = "######Functiomed###### ";
    public final static String VORLAGEN_DIR = "Vorlagen";
    public static boolean isLanguageDE = true;
    public static String settings_domain = "";
    public static String settings_freigabe = ""; // Der Share a oder Share und Pfad a/b/c
    public static String settings_hostname = "";
    public static boolean settings_isSave_local = true; // Speicherung auf Tablet
    public static boolean settings_isSave_network = false; // Speicherung im Netzwerk
    public static String settings_password = "";
    public static String settings_tabletname = "Tablet1";
    public static String settings_template_location = ""; // Tablet oder Netzwerk
    public static String settings_username = "";
    public static float settings_stiftbreite = 1.0f;

    /**
     * Bezug von File Objekten mit Basis Pfad /storage/emulated/0/Download/Secure/Functiomed
     *
     * @param pPath null, a oder a/b/c.txt
     * @return File
     */
    public static File getFile(String pPath) {
        File result = null;
        result = new File(ArDkLib.getSecureFS().getSecurePath() + File.separator + MyUtils.FUNCTIOMED_DIR + File.separator + pPath);
        Log.i(MyUtils.MY_LOG_TAG, "File Path " + result.getPath());
        return result;
    }

    /***/
    public static String getLanguageString() {
        return isLanguageDE ? "DE" : "EN";
    }

    /**
     * Erstellt ein SmbFile mit Benutzerangaben unterhalb smb://hostname/freigabe/".
     * <p>
     * Es wird noch kein Zugriff gemacht (das passiert bei exists() Abfrage oder ähnlichem)
     *
     * @param pPath a/b/foobar.pdf
     * @return SmBFile Objekt auf den Pfad oder die Datei
     */
    public static SmbFile getSmbFile(String pPath) {
        SmbFile result = null;
        Log.i(MY_LOG_TAG, "Start CIFS");

        CIFSContext base = SingletonContext.getInstance();
        CIFSContext authed1 = base.withCredentials(new NtlmPasswordAuthenticator(settings_domain, settings_username, settings_password));

        try {
            result = new SmbFile("smb://" + settings_hostname + "/" + settings_freigabe + "/" + pPath, authed1);
            //SmbFile rootFile = new SmbFile("smb://" + hostname + "/" + freigabe + "/"+ MyAppUtils.getTemplateName(), authed1);
            //  rootFile.exists(); // using the provided credentials

            //Log.i(MyAppUtils.MY_LOG_TAG, "result.getPath() " + result.getPath());
            //Log.i(MyAppUtils.MY_LOG_TAG, "result.exists() " + result.exists());


        } catch (MalformedURLException e) {
            Log.i(MY_LOG_TAG, "SMB Failed " + e.getMessage());
            e.printStackTrace();
        }
        Log.i(MY_LOG_TAG, "CIFS Done");
        return result;
    }

    /**
     * Filename der Vorlage Datei gemäss Sprache EN/DE.
     *
     * @return AnmeldeformularDE.pdf oder AnmeldeformularEN.pdf
     */
    public static String getTemplateName() {
        StringBuffer result = new StringBuffer();
        result.append(ANMELDEFORMULAR_NAME);
        result.append(getLanguageString());
        result.append(".pdf");
        return result.toString();
    }

    /**
     * Navigation entfernen. Hochziehen für erneute Anzeige.
     */
    public static void hideNavbar(AppCompatActivity pActivity) {
        // Navigation entfernen
        pActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void unusedThreadSyntaxExample() {
        // JDK 7
        new Thread() {
            public void run() {
                // your code here ...
            }
        }.start();

        // JDK 8
        Runnable runnable = () -> {
            // your code here ...
        };
        Thread t = new Thread(runnable);
        t.start();
        // In einem eigenen Thread einen neuen UI Thread in Queue einfügen
        /*
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            }
        });
        */
    }
}
