package ch.functiomed.patientenanmeldung;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;

import com.artifex.solib.ArDkLib;
import com.artifex.solib.ConfigOptions;
import com.artifex.solib.FileUtils;
import com.artifex.sonui.editor.Utilities;

import ch.functiomed.patientenanmeldung.mupdf.ClipboardHandler;
import ch.functiomed.patientenanmeldung.mupdf.DataLeakHandlers;
import ch.functiomed.patientenanmeldung.mupdf.PersistentStorage;
import ch.functiomed.patientenanmeldung.mupdf.SecureFS;

/**
 * Startseite für die Functiomed Patientenanmeldung. Enthält Sprachauswahl und Menu für Einstellungen.
 */
public class MainActivity extends AppCompatActivity {

    private static boolean isSetup = false;

    /**
     * Initialisiert die MuPDF Lib
     */
    public static void setupApplicationSpecifics(Context ctx) {
        // create/register handlers (but only once)
        if (!isSetup) {
            Utilities.setDataLeakHandlers(new DataLeakHandlers());
            Utilities.setPersistentStorage(new PersistentStorage());
            ArDkLib.setClipboardHandler(new ClipboardHandler());
            ArDkLib.setSecureFS(new SecureFS());
            FileUtils.init(ctx);
            // Options
            // https://mupdf.com/appkit/docs/mupdf-android-document-options-and-listeners/
            ConfigOptions appCfgOpts = new ConfigOptions();
            appCfgOpts.setEditingEnabled(false);
            appCfgOpts.setPDFAnnotationEnabled(false); // ?
            appCfgOpts.setFormFillingEnabled(false);
            appCfgOpts.setRedactionsEnabled(false); // Form fill
            appCfgOpts.setFullscreenEnabled(false);

            appCfgOpts.setDefaultPdfInkAnnotationDefaultLineColor(0xff0000ff);
            appCfgOpts.setDefaultPdfInkAnnotationDefaultLineThickness(1.5f);

            /*
             * Disable the use of application shared preferences to store document
             * state.
             */
            appCfgOpts.setUsePersistentFileState(false);

            /*
             * Disable things that do not apply to this sample app
             */
            appCfgOpts.setOpenPdfInEnabled(false);
            appCfgOpts.setSaveAsPdfEnabled(false);
            appCfgOpts.setImageInsertEnabled(false);
            appCfgOpts.setPhotoInsertEnabled(false);
            appCfgOpts.setDocAuthEntryEnabled(false);
            appCfgOpts.setTrackChangesFeatureEnabled(false);

            ArDkLib.setAppConfigOptions(appCfgOpts); // ArDkLib anstatt SODKLib

            isSetup = true;
        }
    }

    /**
     * Main Methode der Activity.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(MyUtils.MY_LOG_TAG, "Start MainActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Called when the user taps the functiomedButton
     */
    public void onOpenFunctiomedActivity(View view) {
        Log.i(MyUtils.MY_LOG_TAG, "onOpenFunctiomedActivity");
        Intent intent = new Intent(this, FunctiomedActivity.class);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivity(intent);

    }

    /**
     * Ein Menu wurde ausgewählt
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Log.i(MyUtils.MY_LOG_TAG, "onOptionsItemSelected openSettings action_settings");
                Intent intent = new Intent(this, SettingsActivity.class);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivity(intent);
                return true;
            case R.id.action_about:
                Log.i(MyUtils.MY_LOG_TAG, "onOptionsItemSelected openSettings action_about");
                intent = new Intent(this, AboutActivity.class);
                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivity(intent);
                return true;
            case R.id.action_schliessen:
                Log.i(MyUtils.MY_LOG_TAG, "onOptionsItemSelected openSettings action_schliessen");
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(MyUtils.MY_LOG_TAG, "onStart()");
        //Navigation entfernen
        MyUtils.hideNavbar(this);
        // Initial zurück auf Deutsch stellen
        MyUtils.isLanguageDE = true;
        // GUI neu initialisieren
        setContentView(R.layout.activity_main);

    }

    /**
     * Called when the user taps the switchLanguage. <br>
     * Setzt die Sprache DE/EN
     */
    public void onSwitchLanguage(View view) {
        Log.i(MyUtils.MY_LOG_TAG, "onSwitchLanguage");
        Button functiomedButton = findViewById(R.id.functiomedButton);
        Switch switchLanguage = findViewById(R.id.switchLanguage);
        Log.i(this.getClass().getName(), "switchLanguage isChecked=" + switchLanguage.isChecked());
        if (switchLanguage.isChecked()) {
            MyUtils.isLanguageDE = true;
            switchLanguage.setText("Sprache DE");
            switchLanguage.requestLayout();
            functiomedButton.setText("Neue Anmeldung");
            functiomedButton.requestLayout();
        } else {
            MyUtils.isLanguageDE = false;
            switchLanguage.setText("Language EN");
            switchLanguage.requestLayout();
            functiomedButton.setText("New registration");
            functiomedButton.requestLayout();
        }
    }
}
