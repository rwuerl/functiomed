package ch.functiomed.patientenanmeldung;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class AboutActivity extends AppCompatActivity {

    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        // About Title
        TextView aboutTitle = findViewById(R.id.aboutTitle);
        aboutTitle.setText("Version " + getResources().getString(R.string.app_version));

        // timestamp
        TextView timestamp = findViewById(R.id.timestamp);
        Date now = new Date(BuildConfig.TIMESTAMP);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+02"));
        timestamp.setText("Build vom " + sdf.format(now) + "  " + getResources().getString(R.string.app_revision));

        // API Level Title
        TextView apiLevel = findViewById(R.id.apilevel);
        apiLevel.setText("Android " + Build.VERSION.RELEASE + " API " + Build.VERSION.SDK_INT);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(MyUtils.MY_LOG_TAG, "onStart()");
        //Navigation entfernen
        MyUtils.hideNavbar(this);
    }
}