echo
set version=v1.5
set date_version=%date:~6,4%%date:~3,2%%date:~0,2%_%version%
set src_dir=C:\bitbucket\repos\functiomed\AndroidStudioProjects\FunctiomedPatientenanmeldung\app\build\outputs\apk\debug
set target_dir=C:\Users\Reto\Dropbox\Root\Functiomed Arbeit\FME-2 Elektronsche Unterschrift\SW-Lieferungen\%date_version%
mkdir "%target_dir%"
xcopy /Y/F "%src_dir%\app-debug.apk" "%target_dir%\FunctiomedPatientenanmeldung_%date_version%.apk*"
explorer "%target_dir%"
pause